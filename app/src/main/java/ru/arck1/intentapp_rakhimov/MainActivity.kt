package ru.arck1.intentapp_rakhimov

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val callBtn: Button = findViewById(R.id.button_call)
        val mapBtn: Button = findViewById(R.id.button_map)
        val webBtn: Button = findViewById(R.id.button_web)
        val mailBtn: Button = findViewById(R.id.button_mail)

        callBtn.setOnClickListener {
            onClickCall()
        }
        mapBtn.setOnClickListener {
            onClickViewMap()
        }
        webBtn.setOnClickListener {
            onClickViewWeb()
        }
        mailBtn.setOnClickListener {
            onClickMail()
        }
    }

    private fun isIntentSafe(intent: Intent): Boolean {
        val packageManager = packageManager
        val activities = packageManager.queryIntentActivities(intent, 0)
        return activities.size > 0
    }

    fun onClickCall() {
        val intent = Intent(
            Intent.ACTION_DIAL,
            Uri.parse("tel:${resources.getString(R.string.phone_number)}")
        )


        if (isIntentSafe(intent)) {
            startActivity(intent)
        } else {
            Toast.makeText(
                getApplicationContext(),
                "Your phone have no app can dial",
                Toast.LENGTH_SHORT
            ).show()
        }

    }

    fun onClickViewMap() {
        val intent = Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(resources.getString(R.string.vew_geo))
        }

        if (isIntentSafe(intent)) {
            startActivity(intent)
        } else {
            Toast.makeText(
                getApplicationContext(),
                "Your phone have no app can open map",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    fun onClickViewWeb() {
        val intent = Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(resources.getString(R.string.view_web_url))
        }

        if (isIntentSafe(intent)) {
            startActivity(intent)
        } else {
            Toast.makeText(
                getApplicationContext(),
                "Your phone have no app can web view",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    fun onClickMail() {
        var intent = Intent(
            Intent.ACTION_SENDTO,
            Uri.fromParts(
                "mailto", resources.getString(R.string.email), null
            )
        ).apply {
            putExtra(Intent.EXTRA_SUBJECT, "Teta is test")
            putExtra(Intent.EXTRA_TEXT, resources.getString(R.string.lorem))
        }


        if (isIntentSafe(intent)) {
            startActivity(intent)
        } else {
            Toast.makeText(
                getApplicationContext(),
                "Your phone have no app can send mail",
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}
